<?php


namespace App\Helpers;

class ExternalResources 
{  
    // this file could also be external from an api
    static  $csv_file_url = 'location_data.csv';
    
    static $post_code_api =  'http://api.postcodes.io/postcodes/';

    
    public static function CSVFileUrl() 
    {
        return self::$csv_file_url;
    }

    public static function PostCodeApiUrl()
    {
        return self::$post_code_api;
    }

}