$("#searchShopForm").submit(event => {
    event.preventDefault();

    let postcode = $("#postcode").val();

    if (postcode.length < UK_MIN_POST_CODE_LENGTH) {
        $("#openingTimes").html("<p>Error: incomplete postcode");
        return;
    }

    let xhr = new XMLHttpRequest;
    xhr.open('GET', 'api/GetNearestLocation/' + postcode);
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.addEventListener('load', getNearestLocationResponse);

    xhr.send();

});



function getNearestLocationResponse() {

    let data = JSON.parse(this.responseText);    

    if (data.data) {

        $("#openingTimes").html("<p><h5> Shop Location:</h5>" + data.data.postcode + " " +data.data.constituency+ " " + data.data.district + "</p>");
        $("#openingTimes").append("<p><h5>Opening:</h5></p>");
        let Openings = data.data.opening_hours
        for (var t in Openings) {
            $("#openingTimes").append("<p>" + t + ": " + Openings[t] + "</p>");
        }
    } else {

        $("#openingTimes").html("<p>" + data.error + "</p>");
    }

}