<?php

use Illuminate\Database\Seeder;
use App\Nespresso;
        
class NespressosTableSeeder extends Seeder
{

    /**
     * Seed the nespresso table with data.
     *
     * @return void
     */
    public function run()
    {
        Nespresso::truncate();
        
        Nespresso::create([
            'product_name' => "Ristretto",
            'returns_first_50' => 00.02,
            'returns_51_500' => 00.03,
            'returns_over_500' => 00.05
        ]);
        
        Nespresso::create([
            'product_name' => "Espresso",
            'returns_first_50' => 00.04,
            'returns_51_500' => 00.06,
            'returns_over_500' => 00.10
        ]);
        
        Nespresso::create([
            'product_name' => "Lungo",
            'returns_first_50' => 00.06,
            'returns_51_500' => 00.09,
            'returns_over_500' => 00.15
        ]);

    }

}