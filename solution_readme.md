# Coffee Drop API Solution ReadMe

### I made a few assumptions on the road to developing this solution
1. Two Coffee drop shops are not going to be in the same post code
2. The quantity described as "The first 50 capsules: [Ristretto = 2p, Espresso = 4p, Lungo = 6p]" in the requirements is inclusive.
3. The quantity described as "Capsules 50-500: [Ristetto = 3p, Espresso = 6p Lungo = 9p]" in the requirements = 51-500 inclusive.
4. The currency symbol will be added in the frontend


### Testing with postman
1. body content type: application/json
2. naming convention for POST body properties is exactly the same as in the given CoffeeDrop.postman_collection.json
3. amount of returned coffee pods should be integer


         




