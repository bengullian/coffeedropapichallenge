<?php

namespace App\Helpers;

use App\CoffeeShop;
use App\Helpers\CalculateDistanceBetweenTwoPoints as DistanceCalculator;

class LocateNearestShop 
{  
    /**
     * Returns the postcode nearest to the given codinates.
     *
     * @param  double  $givenLatitude, $givenLongitude
     * @return array
     */

    public static function findNearstShopToCordinates($givenLatitude, $givenLongitude)
    {
        $distances = array();

        $shops = CoffeeShop::all();

        //calculates the distance to every shop in the collection
         foreach ($shops as $shop)
         {
             $distances[$shop->id] = DistanceCalculator::distance($givenLatitude, $givenLongitude, $shop->latitude, $shop->longitude);
         }

         //return the key of the least distance from the calculated $distances
         return array_keys($distances, min($distances), true);
         
    }
}