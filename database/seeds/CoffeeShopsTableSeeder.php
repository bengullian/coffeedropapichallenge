<?php

use Illuminate\Database\Seeder;
use App\Helpers\SeedCoffeeShopsTable;
use App\CoffeeShop;

        
class CoffeeShopsTableSeeder extends Seeder
{

    protected $SeedCoffeeShopsTable;
    
    
    function __construct(SeedCoffeeShopsTable $seedCoffeeShopsTable) 
    { 
        $this->SeedCoffeeShopsTable = $seedCoffeeShopsTable;      
    }


    /**
     * Seed the coffeshop table with data from the csv file from the github repo.
     *
     * @return void
     */
    public function run()
    {
        CoffeeShop::truncate();

        $this->SeedCoffeeShopsTable->run();
         
    }
}
