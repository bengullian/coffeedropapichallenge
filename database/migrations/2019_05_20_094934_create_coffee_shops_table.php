<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoffeeShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coffee_shops', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->text('postcode');

            $table->text('constituency');
            $table->text('district');

            $table->double('latitude', 9, 6);
            $table->double('longitude', 9, 6);

            $table->text('open_Monday');
            $table->text('closed_Monday');

            $table->text('open_Tuesday');
            $table->text('closed_Tuesday');

            $table->text('open_Wednesday');
            $table->text('closed_Wednesday');

            $table->text('open_Thursday');
            $table->text('closed_Thursday');

            $table->text('open_Friday');
            $table->text('closed_Friday');

            $table->text('open_Saturday');
            $table->text('closed_Saturday');

            $table->text('open_Sunday');
            $table->text('closed_Sunday');    
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coffee_shops');
    }
}
