<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nespresso;

class WebController extends Controller
{
    //

    public function extimateCashBack()
    {  
        return view('pages.calculateCashBack', ['shops' => Nespresso::all()]);
    }


    public function createNewLocation() 
    {
        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    
        return view('pages.createShopLocation', ['days' => $days]);
    }
    
}
