<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    protected $statusCode = 200;
    
    public function getStatusCode() {

       return $this->statusCode;
    }


    public function setStatusCode($statusCode) {

        $this->statusCode = $statusCode;

        return $this;
    }


    public function respond($data, $headers = []) {

        return response()->json($data, $this->getStatusCode(), $headers);

    }


    public function respondWithError($message) {
        return response()->json([
            
            'error' => [ 
                'message' => $message,
                'statusCode' => $this->getStatusCode()
            ]

        ]);
    }


}