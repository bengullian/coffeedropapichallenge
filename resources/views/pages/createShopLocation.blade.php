@extends('layouts.app')

@section('content')

<div class="container">
    <br>
    <form id="createStoreForm" action="">
        <div class="form-group row postcode">
            <label for="postcode">New shop postcode:</label>
            <input type="text" name="postcode" id="postcode" placeholder="postcode" value="">
        </div>

        @foreach($days as $day)

        <div class="form-group row">
            <div class="col check">
                <label for="monday"> {{ Str::studly($day) }}:</label>
                <input type="checkbox" name="{{ $day }}" id="check{{ $day }}" value="">
            </div>

            <div class="col">
                <label for="{{ $day }}">Opening:</label>
                <input type="time" name="open{{ $day }}" id="open{{ $day }}" value="00:00">
            </div>

            <div class="col">
                <label for="{{ $day }}">Closing:</label>
                <input type="time" name="close{{ $day }}" id="close{{ $day }}" value="00:00">
            </div>
        </div>

        @endforeach

        <div class="form-group row">
            <div class="col col3" id="errorMessage"></div>
            <div class="col col2">
                <input type="submit" value="Submit">
            </div>
        </div>

    </form>

</div>

@endsection



