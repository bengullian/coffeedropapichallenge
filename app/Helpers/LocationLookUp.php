<?php


namespace App\Helpers;

class LocationLookUp 
{   
    /* returns the location data of the shop using the supplied postcode 
    *   with status of the request or error status when the request fails
    */
    public static function getLocationFromPostCode($post_code_api, $postcode) : array
    {
        $request = curl_init();
        $curlConfig = array(
        CURLOPT_URL      => $post_code_api.$postcode,
        CURLOPT_HTTPGET   => true,
        CURLOPT_RETURNTRANSFER => true    
        );

        curl_setopt_array($request, $curlConfig);
        $payload = json_decode(curl_exec($request));
        $status = curl_getinfo($request, CURLINFO_HTTP_CODE); 

        curl_close($request);

        if ($status == 200) {
            
            return(array(
                "status" => $status,
                "latitude" => $payload->result->latitude,
                "longitude" => $payload->result->longitude,
                "constituency" => $payload->result->parliamentary_constituency,
                "district" => $payload->result->admin_district
            ));
        }

        return(array("status" => $status));    

    }


}