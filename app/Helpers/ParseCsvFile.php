<?php


namespace App\Helpers;


class ParseCsvFile 
{    

    public static function getCsVDataRows($csv_file_url) : array
    {
        //read the contents of the csv file
        $csv_data = file_get_contents($csv_file_url); 

        //get the rows of data with header included
        $data_rows = explode("\n", $csv_data);

        return $data_rows;

    }    
}