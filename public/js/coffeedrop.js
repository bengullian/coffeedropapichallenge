const UK_MIN_POST_CODE_LENGTH = 6;

function sendHttpPostRequest(address, payload) {

    return new Promise((resolve, reject) => {

        let xhr = new XMLHttpRequest;

        xhr.open('POST', address);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                resolve(xhr.responseText);
            }
        }
        xhr.send(JSON.stringify(payload));
    })

}