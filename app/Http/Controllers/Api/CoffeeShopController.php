<?php

namespace App\Http\Controllers\Api;

use App\CoffeeShop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\ExternalResources;
use App\Helpers\LocationLookUp;
use App\Helpers\LocateNearestShop;
use App\Http\Controllers\ApiController;
use App\Helpers\ValidateCreateShopPost;
use App\Helpers\PersistShopPostRequest;
use App\Http\Resources\CoffeeShop as CoffeeShopResource;



class CoffeeShopController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->respondWithError("Not authorised");
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = ValidateCreateShopPost::validatePost($request);

        if ($post->fails()) {
            return $this->respondWithError($post->errors());;
        }

       return PersistShopPostRequest::createNewShop($request);

    }




    /**
     * Returns the address and opening times of the closest shop to the given postcode from resources.
     *
     * @param  string  $postcode
     * @return \Illuminate\Http\Response
     */
    public function show($postcode)
    {
        if(strlen($postcode) > 4)
        {

            $location =  LocationLookUp::getLocationFromPostCode(ExternalResources::PostCodeApiUrl(), $postcode);

         if ($location["status"] == 200)
         {
            $nearest = LocateNearestShop::findNearstShopToCordinates($location["latitude"], $location["longitude"]);

            return new CoffeeShopResource(CoffeeShop::find($nearest[0]));
         }

        }

         return $this->respondWithError("postcode not valid");
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->respondWithError("not authorised");
        
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->respondWithError("not authorised");
    }
}
