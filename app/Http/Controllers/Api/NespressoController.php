<?php

namespace App\Http\Controllers\Api;


use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\CalculateCashBack;
use App\Http\Controllers\ApiController;

class NespressoController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->respondWithError("not authorised");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->respondWithError("not authorised");
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->respondWithError("not authorised");
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->respondWithError("not authorised");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->respondWithError("not authorised");
    }


    public function cashBack(Request $request)
    {
        // validate the request
        $post = Validator::make($request->all(), [
            'Ristretto' => 'sometimes|required|integer',
            'Espresso' => 'sometimes|required|integer',
            'Lungo' => 'sometimes|required|integer'           
        ]);  
        

        if ($post->fails()) {

            return $this->respondWithError($post->errors());
            
        }       
        
       return ["status" => 200, "cashback" => number_format(CalculateCashBack::fromPostRequest($request), 2)];

    }

}
