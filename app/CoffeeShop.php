<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoffeeShop extends Model
{
    protected $table = 'coffee_shops';
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


     protected $fillable = [
        'postcode', 'latitude', 'longitude', 'constituency', 'district', 'open_Monday', 'open_Tuesday', 
        'open_Wednesday', 'open_Thursday','open_Friday', 'open_Saturday', 'open_Sunday', 
        'closed_Monday', 'closed_Tuesday', 'closed_Wednesday', 'closed_Thursday',
        'closed_Friday', 'closed_Saturday', 'closed_Sunday',
    ]; 


    public function toArray() {
        return [
            "postcode" => $this->postcode,
            "constituency" => $this->constituency,
            "district" => $this->district,
            "opening_hours" => [
                "Monday" => $this->filterAndMerge($this->open_Monday, $this->closed_Monday),
                "Tuesday" => $this->filterAndMerge($this->open_Tuesday, $this->closed_Tuesday),
                "Wednesday" => $this->filterAndMerge($this->open_Wednesday, $this->closed_Wednesday),
                "Thursday" => $this->filterAndMerge($this->open_Thursday , $this->closed_Thursday),
                "Friday" => $this->filterAndMerge($this->open_Friday, $this->closed_Friday),
                "Saturday" => $this->filterAndMerge($this->open_Saturday, $this->closed_Saturday),
                "Sunday" => $this->filterAndMerge($this->open_Sunday, $this->closed_Sunday),
            ]

        ];
    }


    /**
     * returns "" when $time is an empty string else return $time. Strips any return characters
     * @param string $Time, $Time
     * @return string
     */
    protected function filterAndMerge($open_time, $close_time)
    {
        if ($open_time == "" || $close_time == "") 
        {
            return "closed";
        }
        
        return   "$open_time - $close_time";
    }


}
   