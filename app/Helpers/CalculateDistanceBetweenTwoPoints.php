<?php


namespace App\Helpers;


class CalculateDistanceBetweenTwoPoints
{

    /**
     * Calculate the distance between two points.
     * using Haversine Formula for calculating distances from latitude and longitude
     * 
     * @param  double  $lat1, $lon1, $lat2, $lon2
     * @return double $distance_miles, the distance between the two points in miles
     */
    public static function distance($lat1, $lon1, $lat2, $lon2) {

        //converts lat and lon from degress to radians
        $pi80 = M_PI / 180;
        $lat1 *= $pi80; 
        $lon1 *= $pi80;
        $lat2 *= $pi80;
        $lon2 *= $pi80;

        $r = 6372.797; // mean radius of Earth in km
        $dlat = $lat2 - $lat1;
        $dlon = $lon2 - $lon1;
        
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        
        $distance_km =  $r * $c;
        $distance_miles = 0.621371 * $distance_km;
        
        return $distance_miles;
    
    }

}