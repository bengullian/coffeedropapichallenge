@extends('layouts.app')

@section('content')

<div class="container homeContainer">

    <div class="slogan" id="Slogan">

        <p>Find a CoffeeDrop shop close to you to start cashing in!</p>

        <img src="https://www.futurebrand.com/uploads/case-studies/_sliderHd/Nespresso_15_8853_Main_B_R3.jpg" />
    </div>

    <div class="sidebar">
        <div class="openingTimes" id="openingTimes"></div>

        <div class="searchContainer" id="searchShopContainer">
            <form id="searchShopForm" action="">
                <div class="form-group row postcode">
                    <input type="text" name="postcode" id="postcode" placeholder="enter post code" value="">
                </div>
                <button type="submit" class="btn btn-outline-primary">Submit</button>
            </form>
        </div>
    </div>


</div>

@endsection



