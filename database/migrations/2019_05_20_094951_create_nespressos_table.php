<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNespressosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nespressos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('product_name');
            $table->decimal('returns_first_50', 4, 2); 
            $table->decimal('returns_51_500', 4, 2);  
            $table->decimal('returns_over_500', 4, 2);           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nespressos');
    }
}
