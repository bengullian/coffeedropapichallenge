@extends('layouts.app')

@section('content')

<div class="container cashback">
    <h3>Cash Back</h3>
    <b id="cachBackAmount"></b>
<div class="row justify-content-center">
    <form id="cashBackForm">
        <div class="list-group list-group-item-action">
            @foreach ($shops as $shop)
            <div class="list-group-item justify-content-between">
                <span class="badge badge-default badge-pill">
                    <h6>{{ $shop->product_name }}</h6>
                </span>
                <input type="number" id="{{ $shop->product_name }}" min="0" autofocus value="0">
            </div>
            @endforeach
        </div>
        <button type="submit" class="btn btn-outline-primary">Submit</button>
    </form>
</div>
</div>

@endsection



