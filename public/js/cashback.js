$("#cashBackForm").submit((event) => {

    event.preventDefault();

    let payload = {
        Ristretto: $("#Ristretto").val(),
        Espresso: $("#Espresso").val(),
        Lungo: $("#Lungo").val()
    };

    sendHttpPostRequest('/api/CalculateCashback', payload)
        .then((response) => {
            displayCashBackResponse(response);
        });
});


function displayCashBackResponse(response) {

    $("#cachBackAmount").html('£' + JSON.parse(response).cashback);

}