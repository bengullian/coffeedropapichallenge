<?php

namespace App\Helpers;

use Validator;
use Illuminate\Http\Request;

class ValidateCreateShopPost
{

    public static function validatePost(Request $request)
    {
        $post = Validator::make($request->all(), [
            'postcode' => 'bail|unique:coffee_shops|required|max:10',
            'opening_times' => 'required',
            'closing_times' => 'required',           
        ]);        

        return $post;
    }
}