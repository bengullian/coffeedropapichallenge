<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nespresso extends Model
{
    protected $table = 'nespressos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_name', 'returns_first_50', 'returns_50_500', 'returns_over_500'
    ];
}
