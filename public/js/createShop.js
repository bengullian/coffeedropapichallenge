$("#createStoreForm").submit((event) => {
    event.preventDefault();
    $('#errorMessage').html("");

    let payload = {
        opening_times: {},
        closing_times: {}
    };

    // if string length of UK postcode is not valid
    if ($('#postcode').val().toString().length < UK_MIN_POST_CODE_LENGTH) {

        $('#errorMessage').html("Enter a valid UK postcode!");
        return;
    }

    payload.postcode = $('#postcode').val();
    payload = validateCreateShopForm(payload);

    if (payload == false) {
        return;
    } else if (Object.keys(payload.opening_times).length == 0 || Object.keys(payload.closing_times).length == 0) {
        $('#errorMessage').html("Opening and closing times are missing");
        return;
    }

    sendHttpPostRequest('/api/CreateNewLocation', payload)
        .then((response) => {
            handleRequestResponse(response);
        });

});



function validateCreateShopForm(payload) {

    let days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

    for (let d of days) {
        let opening = $('#open' + d).val();
        let closing = $('#close' + d).val();

        //extract the hrs part of time
        let hrsO = opening.split(':')[0];
        let hrsC = closing.split(':')[0];

        //closing time is earlier than opening time
        if (parseInt(hrsC) < parseInt(hrsO)) {
            $('#errorMessage').html("a closing time can not be earlier than opening time");
            return false;
        }

        //checkbox is not selected but opening or closing time is
        else if (!$('#check' + d).is(':checked') && opening != closing) {
            $('#errorMessage').html("validation failed for :" + d + ", the checkbox is not selected");
            return false;
        }
        //nothing is selected or the opening and closing times are same
        else if ($('#check' + d).is(':checked') && opening == closing) {
            $('#errorMessage').html("validation failed for :" + d + ", opening and closing times cannot be the same");
            return false;
        }

        //the checkbox selected with opening and closing times
        if ($('#check' + d).is(':checked') && opening != closing) {
            payload.opening_times[d] = $('#open' + d).val();
            payload.closing_times[d] = $('#close' + d).val();
        }

    }

    return payload;
}


function handleRequestResponse(message) {

    let response = JSON.parse(message);

    if (response["error"]) {
        let errorMessage = response["error"].postcode || response["error"].opening_times || response["error"].closing_times;
        $('#errorMessage').html("Error: " + errorMessage[0]);
        return;
    }

    $('#errorMessage').html(response.response);
    document.getElementById("createStoreForm").reset();

}