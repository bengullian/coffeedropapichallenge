<?php

namespace App\Helpers;

use App\CoffeeShop;
use Illuminate\Http\Request;

use App\Helpers\ExternalResources;
use App\Helpers\LocationLookUp;

class PersistShopPostRequest
{

    public static function createNewShop(Request $request)
    {
        $storeLocation = LocationLookUp::getLocationDetailsFromPostCode(ExternalResources::PostCodeApiUrl(), request('postcode'));

        if ($storeLocation["status"] == 200) 
        {
            CoffeeShop::create([
                'postcode' => request('postcode'),

                'latitude' => $storeLocation["latitude"],
                'longitude' => $storeLocation["longitude"],

                'constituency' => $storeLocation["constituency"],
                'district' => $storeLocation["district"],

                'open_Monday' =>  request('opening_times.monday') ?? "",
                'open_Tuesday' => request('opening_times.tuesday') ?? "",

                'open_Wednesday' => request('opening_times.wednesday') ?? "",
                'open_Thursday' => request('opening_times.thursday') ?? "",

                'open_Friday' => request('opening_times.friday') ?? "",
                'open_Saturday' => request('opening_times.saturday') ?? "",

                'open_Sunday' => request('opening_times.sunday') ?? "",
                'closed_Monday' => request('closing_times.monday') ?? "",

                'closed_Tuesday' => request('closing_times.tuesday') ?? "",
                'closed_Wednesday' => request('closing_times.wednesday') ?? "",

                'closed_Thursday' => request('closing_times.thurday') ?? "",
                'closed_Friday' => request('closing_times.friday') ?? "",

                'closed_Saturday' => request('closing_times.saturday') ?? "",
                'closed_Sunday' => request('closing_times.sunday') ?? "",
            ]);

            return ["response" => "Store creation successful"];
        }

        return ["status" => $storeLocation["status"], "error" => "invalid postcode"];

    }

}