<?php


namespace App\Helpers;

use App\CoffeeShop;
use App\Helpers\ParseCsvFile;
use App\Helpers\LocationLookUp;
use App\Helpers\ExternalResources;


/**
 * Helper class for seeding the CoffeeShops table from a the given csv file in the root folder
 * in this case the filename is location_data.csv
*/
class SeedCoffeeShopsTable
{

    public function run()
    {
        //  get an array of the records in the csv file
        $data_rows = ParseCsvFile::getCsVDataRows(ExternalResources::CSVFileUrl());
         
        //  get the header row 
        $headers = array_shift($data_rows);

        $headers_array = explode(',', $headers);

        //trim the last header for unwanted characters
        $headers_array[count($headers_array) - 1] = trim($headers_array[count($headers_array ) - 1]);        

        //  loop through the rows of data and persist each row to the database
        foreach($data_rows as $row) { 

            $cols = explode(",", $row); 

            // to ensure we are dealing with a row containing data
            if (count($cols) == count($headers_array)) 
            {
                $cols[count($cols) - 1] = trim($cols[count($cols) - 1]);
                 //  create an associative array using the headers and the row data
                $row_to_persist = array_combine($headers_array, $cols);

          
                //  $hop data is a combination of location data and opening and closing times from csv file
                // get location data and merge it with row data from csv file

                $location_data = LocationLookUp::getLocationFromPostCode(ExternalResources::PostCodeApiUrl(), $row_to_persist['postcode']);            

                if ($location_data['status'] == 200) {
                
                    // remove status from the data to be persisted
                    unset($location_data['status']);

                    $shop_data = array_merge($location_data, $row_to_persist);
                    CoffeeShop::create($shop_data);

                }  
            }    
   
        }

    }   

}