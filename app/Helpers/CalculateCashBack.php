<?php

namespace App\Helpers;

use App\Nespresso as Nespresso;
use Illuminate\Http\Request;

class CalculateCashBack
{

     /**
     * Calculates and returns the cashback value from the request params.
     *
     * @param  Request  $request
     * @return double
     */
    public static function fromPostRequest(Request $request)
    {
        $qty_Ristretto = request('Ristretto')  ?? 0;
        $qty_Espresso = request('Espresso') ?? 0;
        $qty_Lungo = request('Lungo') ?? 0;
        
        return self::ristrettoCashBack($qty_Ristretto) + self::espressoCashBack($qty_Espresso) + self::lungoCashBack($qty_Lungo);
    }


    /**
     * 
     * Each product has a column in the database for the value of returns first_50, 51-500, above 500.
     * return the value for the range * qty
     *  @param  int  $qty_Ristretto
     *  @return double
     */
    protected static function ristrettoCashBack($qty_Ristretto) 
    {
        if ($qty_Ristretto <= 50) {
            return $qty_Ristretto * Nespresso::where('product_name', "Ristretto")->first()->returns_first_50;
        }

        else if ($qty_Ristretto > 50 && $qty_Ristretto <= 500) {
            return $qty_Ristretto * Nespresso::where('product_name', "Ristretto")->first()->returns_51_500;
        }

        else if ($qty_Ristretto > 500) {
            return $qty_Ristretto * Nespresso::where('product_name', "Ristretto")->first()->returns_over_500;
        }
    }


    protected static function espressoCashBack($qty_Espresso)
    {
        if ($qty_Espresso <= 50) {
            return $qty_Espresso * Nespresso::where('product_name', "Espresso")->first()->returns_first_50;
        }

        else if ($qty_Espresso > 50 && $qty_Espresso <= 500) {
            return $qty_Espresso * Nespresso::where('product_name', "Espresso")->first()->returns_51_500;
        }

        else if ($qty_Espresso > 500) {
            return $qty_Espresso * Nespresso::where('product_name', "Espresso")->first()->returns_over_500;
        }
    }
    

    protected static function lungoCashBack($qty_Lungo)
    {
        if ($qty_Lungo <= 50) {
            return $qty_Lungo * Nespresso::where('product_name', "Lungo")->first()->returns_first_50;
        }

        else if ($qty_Lungo > 50 && $qty_Lungo <= 500) {
            return $qty_Lungo * Nespresso::where('product_name', "Lungo")->first()->returns_51_500;
        }

        else if ($qty_Lungo > 500) {
            return $qty_Lungo * Nespresso::where('product_name', "Lungo")->first()->returns_over_500;
        }
    }


}